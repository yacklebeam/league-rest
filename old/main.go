package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	_ "github.com/gorilla/handlers"
)

var db *sql.DB

func checkError(e error) bool {
	if e != nil {
		log.Fatal(e)
		fmt.Println(e)
		return true
	}
	return false
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("usage: league-rest.exe user/pass@host/dbname")
		return
	}

	argsString := os.Args[1]
	argsString = strings.Replace(argsString, "@", "/", -1)

	parts := strings.Split(argsString, "/")
	if len(parts) < 4 {
		fmt.Println("usage: league-rest.exe user/pass@host/dbname")
		return
	}

	connStr := "user=" + parts[0] + " password=" + parts[1] + " dbname=" + parts[3] + " host=" + parts[2] + " sslmode=disable"

	db = connectToDB(connStr)
	if db == nil {
		fmt.Println("Error with Database")
		fmt.Println("REST Server running in error mode")
	}

	router := newRouter()
	fmt.Println("GOREST Server running on port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
