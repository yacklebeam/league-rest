package main

type payload interface {
}

type region struct {
	ID         string `json:"id"`
	RegionName string `json:"region_name"`
	SelfLink   string `json:"self"`
}

type team struct {
	ID       string `json:"id"`
	TeamName string `json:"team_name"`
	RegionID string `json:"region_id"`
	SelfLink string `json:"self"`
}

type tournament struct {
	ID       int    `json:"id"`
	RegionID string `json:"region_id"`
	Year     string `json:"year"`
	Split    string `json:"split"`
}

type ban struct {
	ID         int    `json:"id"`
	GameID     int    `json:"game_id"`
	Side       string `json:"side"`
	BanNumber  int    `json:"ban_number"`
	ChampionID string `json:"champion_id"`
}

type pick struct {
	ID         int    `json:"id"`
	GameID     int    `json:"game_id"`
	Side       string `json:"side"`
	PickNumber int    `json:"pick_number"`
	ChampionID string `json:"champion_id"`
	Role       string `json:"role"`
}

type gameDetail struct {
	Side     string  `json:"side"`
	TeamID   string  `json:"team_id"`
	Gold     float64 `json:"gold"`
	Kills    int     `json:"kills"`
	Towers   int     `json:"towers"`
	Inhibs   int     `json:"inhibs"`
	Barons   int     `json:"barons"`
	Dragons  int     `json:"dragons"`
	GameTime int     `json:"game_time"`
}

type gameOld struct {
	ID         int        `json:"id"`
	MatchID    int        `json:"match_id"`
	GameNumber int        `json:"game_number"`
	Winner     string     `json:"winner"`
	GameTime   int        `json:"game_time"`
	BlueSide   gameDetail `json:"blue_side"`
	RedSide    gameDetail `json:"red_side"`
}

type match struct {
	ID           int        `json:"id"`
	TeamOneID    string     `json:"team_one_id"`
	TeamTwoID    string     `json:"team_two_id"`
	TournamentID string     `json:"tournament_id"`
	MatchType    string     `json:"match_type"`
	GameCount    int        `json:"max_games"`
	Week         int        `json:"week_no"`
	Games        []game     `json:"games"`
	Tournament   tournament `json:"tournament"`
}

type regionList struct {
	Data    []region `json:"data"`
	Count   int      `json:"count"`
	StartAt int      `json:"start_at"`
}

type teamList struct {
	Data    []team `json:"data"`
	Count   int    `json:"count"`
	StartAt int    `json:"start_at"`
}

type matchList struct {
	Data    []match `json:"data"`
	Count   int     `json:"count"`
	StartAt int     `json:"start_at"`
}

type tournamentList struct {
	Data    []tournament `json:"data"`
	Count   int          `json:"count"`
	StartAt int          `json:"start_at"`
}

type gameList struct {
	Data    []game `json:"data"`
	Count   int    `json:"count"`
	StartAt int    `json:"start_at"`
}

type clistValue struct {
	ID    string `json:"id"`
	Value string `json:"value"`
}

////////////////////////////////////////////////
//	Game Struct
//	1 Game Main
//	- 2x Game Stats
//	-- 2x Game Bans
//	-- 2x Game Picks
////////////////////////////////////////////////
type game struct {
	ID         int        `json:"id"`
	MatchID    int        `json:"match_id"`
	GameNumber int        `json:"game_number"`
	Winner     string     `json:"winner"`
	GameTime   int        `json:"game_time"`
	BlueSide   gameStats  `json:"blue_side"`
	RedSide    gameStats  `json:"red_side"`
	Tournament tournament `json:"tournament"`
}

type gameStats struct {
	GameID  int        `json:"game_id"`
	Side    string     `json:"side"`
	TeamID  string     `json:"team_id"`
	Gold    float32    `json:"gold"`
	Kills   int        `json:"kills"`
	Towers  int        `json:"towers"`
	Inhibs  int        `json:"inhibs"`
	Barons  int        `json:"barons"`
	Dragons int        `json:"dragons"`
	Bans    []gameBan  `json:"bans"`
	Picks   []gamePick `json:"picks"`

	GameTime int  `json:"game_time"`
	IsWinner bool `json:"is_winner"`
}

type gameBan struct {
	GameID     int    `json:"game_id"`
	Side       string `json:"side"`
	BanNumber  int    `json:"ban_number"`
	ChampionID string `json:"champion_id"`
}

type gamePick struct {
	GameID     int    `json:"game_id"`
	Side       string `json:"side"`
	PickNumber int    `json:"pick_number"`
	Role       string `json:"role"`
	ChampionID string `json:"champion_id"`
	PlayerID   string `json:"player_id"`
}

type banRanking struct {
	ChampionID string  `json:"champion_id"`
	BanCount   int     `json:"ban_count"`
	BanAvg     float32 `json:"ban_average"`
	BanScore   float32 `json:"ban_score"`
}
