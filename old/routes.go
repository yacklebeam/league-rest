package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

type route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type routes []route

func newRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range restRoutes {
		router.
			Methods(route.Method).
			Path("/api" + route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	return router
}

var restRoutes = routes{
	route{
		"RegionIndex",
		"GET",
		"/region",
		checkForDatabase(regionIndex),
	},
	route{
		"RegionLookup",
		"GET",
		"/region/{region}",
		checkForDatabase(regionLookup),
	},
	route{
		"RegionLookupTeams",
		"GET",
		"/region/{region}/team",
		checkForDatabase(teamLookupByRegion),
	},
	route{
		"TeamIndex",
		"GET",
		"/team",
		checkForDatabase(teamIndex),
	},
	route{
		"TeamLookup",
		"GET",
		"/team/{team}",
		checkForDatabase(teamLookup),
	},
	route{
		"InsertMatch",
		"POST",
		"/match",
		checkForDatabase(verifyAuthentication(matchInsert)),
	},
	route{
		"InsertMatchOption",
		"OPTIONS",
		"/match",
		handleOptions,
	},
	route{
		"UpdateMatch",
		"PUT",
		"/match/{match}",
		checkForDatabase(matchUpdate),
	},
	route{
		"MatchIndex",
		"GET",
		"/match",
		checkForDatabase(matchIndex),
	},
	route{
		"TournamentIndex",
		"GET",
		"/tournament",
		checkForDatabase(tournamentIndex),
	},
	route{
		"CListLookup",
		"GET",
		"/clist/{name}",
		checkForDatabase(clistLookup),
	},
	// route{
	// 	"InsertGame",
	// 	"POST",
	// 	"/game",
	// 	checkForDatabase(verifyAuthentication(gameInsert)),
	// },
	route{
		"GameIndex",
		"GET",
		"/game",
		checkForDatabase(gameIndex),
	},
	route{
		"BanRankings",
		"GET",
		"/ban_ranking",
		checkForDatabase(getBanRankingsList),
	},
	route{
		"BanIndex",
		"GET",
		"/ban",
		checkForDatabase(banIndex),
	},
}
