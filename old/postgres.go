package main

import (
	"database/sql"
	"errors"
	"fmt"

	_ "github.com/lib/pq"
)

func connectToDB(connStr string) *sql.DB {
	db, err := sql.Open("postgres", connStr)
	if checkError(err) {
		return nil
	}
	fmt.Println("Database connected")
	return db
}

func getUserHash(username string) string {
	var hash string
	_ = db.QueryRow("SELECT password_hash FROM users WHERE userid=$1", username).Scan(&hash)
	return hash
}

func getAllRegions() regionList {
	var rs regionList
	rs.Count = 0
	rs.StartAt = 0

	rows, err := db.Query("SELECT id, region_name FROM region_main")
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var r region
		err = rows.Scan(&r.ID, &r.RegionName)
		if !checkError(err) {
			r.SelfLink = "/api/region/" + r.ID
			rs.Data = append(rs.Data, r)
			rs.Count++
		}
	}

	return rs
}

func getRegion(id string) region {
	var r region
	_ = db.QueryRow("SELECT id, region_name FROM region_main WHERE id=$1", id).Scan(&r.ID, &r.RegionName)
	r.SelfLink = "/api/region/" + r.ID
	return r
}

func getAllTeams() teamList {
	var ts teamList
	ts.Count = 0
	ts.StartAt = 0

	rows, err := db.Query("SELECT id, team_name, region_id FROM team_main")
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var t team
		err = rows.Scan(&t.ID, &t.TeamName, &t.RegionID)
		if !checkError(err) {
			t.SelfLink = "/api/team/" + t.ID
			ts.Data = append(ts.Data, t)
			ts.Count++
		}
	}

	return ts
}

func getTeam(id string) team {
	var t team
	_ = db.QueryRow("SELECT id, team_name, region_id FROM team_main WHERE id=$1", id).Scan(&t.ID, &t.TeamName, &t.RegionID)
	t.SelfLink = "/api/team/" + t.ID
	return t
}

func getTeamsByRegion(id string) teamList {
	var ts teamList
	ts.Count = 0
	ts.StartAt = 0

	rows, err := db.Query("SELECT id, team_name, region_id FROM team_main WHERE region_id=$1", id)
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var t team
		err = rows.Scan(&t.ID, &t.TeamName, &t.RegionID)
		if !checkError(err) {
			t.SelfLink = "/api/team/" + t.ID
			ts.Data = append(ts.Data, t)
			ts.Count++
		}
	}

	return ts
}

func matchExists(teamOne, teamTwo string, weekNo int, tournamentID string) bool {
	count := 0
	_ = db.QueryRow("SELECT COUNT(*) FROM match_main WHERE week=$3 AND tournament_id=$4 AND ((team_one_id=$1 AND team_two_id=$2) OR (team_one_id=$2 AND team_two_id=$1))", teamOne, teamTwo, weekNo, tournamentID).Scan(&count)
	return count > 0
}

func insertMatch(m match) (int, error) {

	if matchExists(m.TeamOneID, m.TeamTwoID, m.Week, m.TournamentID) {
		return -1, errors.New("Match already exists")
	}

	qString := "INSERT INTO match_main (team_one_id, team_two_id, tournament_id, match_type, week, game_count) VALUES ($1,$2,$3,$4,$5,$6) RETURNING id;"
	var mID int
	err := db.QueryRow(qString, m.TeamOneID, m.TeamTwoID, m.TournamentID, m.MatchType, m.Week, m.GameCount).Scan(&mID)

	if err != nil {
		fmt.Println(err)
		return -1, err
	}

	return mID, nil
}

func getMatch(id string) match {
	var m match
	_ = db.QueryRow("SELECT id,team_one_id,team_two_id,tournament_id,match_type,game_count,week FROM match_main WHERE id=$1", id).Scan(&m.ID, &m.TeamOneID, &m.TeamTwoID, &m.TournamentID, &m.GameCount, &m.Week)
	return m
}

func getAllMatches() matchList {
	var ms matchList
	ms.Count = 0
	ms.StartAt = 0

	rows, err := db.Query("SELECT match_main.id,team_one_id,team_two_id,tournament_id,match_type,game_count,week,tournament_main.region_id,tournament_main.year,tournament_main.split FROM match_main JOIN tournament_main ON (match_main.tournament_id = tournament_main.id)")
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var m match
		err = rows.Scan(&m.ID, &m.TeamOneID, &m.TeamTwoID, &m.TournamentID, &m.MatchType, &m.GameCount, &m.Week, &m.Tournament.RegionID, &m.Tournament.Year, &m.Tournament.Split)
		if !checkError(err) {
			ms.Data = append(ms.Data, m)
			ms.Count++
		}
	}

	return ms
}

func getAllTournaments() tournamentList {
	var ts tournamentList
	ts.Count = 0
	ts.StartAt = 0

	rows, err := db.Query("SELECT id, region_id, year, split FROM tournament_main")
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var t tournament
		err = rows.Scan(&t.ID, &t.RegionID, &t.Year, &t.Split)
		if !checkError(err) {
			ts.Data = append(ts.Data, t)
			ts.Count++
		}
	}

	return ts
}

func getClistValues(name string) []clistValue {
	var clistValues []clistValue
	qString := "SELECT id,value FROM clist_" + name
	rows, err := db.Query(qString)
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var c clistValue
		err = rows.Scan(&c.ID, &c.Value)
		if !checkError(err) {
			clistValues = append(clistValues, c)
		}
	}

	return clistValues
}

// func insertGame(g game) (int, error) {

// 	qString := "INSERT INTO game_main (match_id, game_number, side, team_id, winner, game_time, gold, kills, towers, inhibs, barons, dragons) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING id;"
// 	var gID int
// 	err := db.QueryRow(qString, g.MatchID, g.GameNumber, g.Side, g.TeamID, g.Winner, g.GameTime, g.Gold, g.Kills, g.Towers, g.Inhibs, g.Barons, g.Dragons).Scan(&gID)

// 	if err != nil {
// 		fmt.Println(err)
// 		return -1, err
// 	}

// 	banQString := "INSERT INTO game_bans (game_id, side, ban_number, champion_id) VALUES ($1, $2, $3, $4)"
// 	for _, b := range g.Bans {
// 		err := db.QueryRow(banQString, gID, b.Side, b.BanNumber, b.ChampionID)

// 		if err != nil {
// 			fmt.Println(err)
// 		}
// 	}

// 	pickQString := "INSERT INTO game_picks (game_id, side, ban_number, champion_id, role) VALUES ($1, $2, $3, $4, $5)"
// 	for _, p := range g.Picks {
// 		err := db.QueryRow(pickQString, gID, p.Side, p.PickNumber, p.ChampionID, p.Role)

// 		if err != nil {
// 			fmt.Println(err)
// 		}
// 	}

// 	return gID, err
// }

func getAllGames() gameList {
	var gs gameList
	gs.Count = 0
	gs.StartAt = 0

	rows, err := db.Query("SELECT game_main.id, match_id, game_number, winner, game_time, tournament_main.id, tournament_main.region_id, tournament_main.year, tournament_main.split FROM game_main JOIN match_main ON game_main.match_id = match_main.id JOIN tournament_main ON tournament_main.id = match_main.tournament_id")
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var g game
		err = rows.Scan(&g.ID, &g.MatchID, &g.GameNumber, &g.Winner, &g.GameTime, &g.Tournament.ID, &g.Tournament.RegionID, &g.Tournament.Year, &g.Tournament.Split)
		if !checkError(err) {
			g.BlueSide = getStats(g.ID, "BLUE")
			g.RedSide = getStats(g.ID, "RED")

			blueSideWinner := (g.Winner == g.BlueSide.TeamID)

			g.BlueSide.GameTime = g.GameTime
			g.BlueSide.IsWinner = blueSideWinner
			g.RedSide.GameTime = g.GameTime
			g.RedSide.IsWinner = !blueSideWinner

			g.BlueSide.Bans = getBans(g.ID, "BLUE")
			g.RedSide.Bans = getBans(g.ID, "RED")

			g.BlueSide.Picks = getPicks(g.ID, "BLUE")
			g.RedSide.Picks = getPicks(g.ID, "RED")

			gs.Data = append(gs.Data, g)
			gs.Count++
		}
	}

	return gs
}

func getStats(gameID int, side string) gameStats {
	var g gameStats
	statQuery := "SELECT side, team_id, gold, kills, towers, inhibs, barons, dragons FROM game_stats WHERE game_id = $1 AND side = $2"
	_ = db.QueryRow(statQuery, gameID, side).Scan(&g.Side, &g.TeamID, &g.Gold, &g.Kills, &g.Towers, &g.Inhibs, &g.Barons, &g.Dragons)

	return g
}

func getBans(gameID int, side string) []gameBan {
	var bans []gameBan
	qString := "SELECT game_id, side, ban_number, champion_id FROM game_bans WHERE game_id = $1 AND side = $2"
	rows, err := db.Query(qString, gameID, side)
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var b gameBan
		err = rows.Scan(&b.GameID, &b.Side, &b.BanNumber, &b.ChampionID)
		if !checkError(err) {
			bans = append(bans, b)
		}
	}

	return bans
}

func getPicks(gameID int, side string) []gamePick {
	var picks []gamePick
	qString := "SELECT game_id, side, pick_number, champion_id, role, player FROM game_picks WHERE game_id = $1 AND side = $2"
	rows, err := db.Query(qString, gameID, side)
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var p gamePick
		err = rows.Scan(&p.GameID, &p.Side, &p.PickNumber, &p.ChampionID, &p.Role, &p.PlayerID)
		if !checkError(err) {
			picks = append(picks, p)
		}
	}

	return picks
}

func getBanRankings(whereStr string) []banRanking {
	var bs []banRanking

	selectStr := "select champion_id, avg(ban_number), count(ban_number), count(ban_number) / avg(ban_number)"
	fromStr := "from game_bans gb	JOIN game_main gm on gb.game_id = gm.id	JOIN match_main mm on gm.match_id = mm.id JOIN tournament_main tm on mm.tournament_id = tm.id JOIN game_stats gs on gm.id = gs.game_id AND gb.side = gs.side"
	groupStr := "group by champion_id"

	qString := selectStr + " " + fromStr + " " + whereStr + " " + groupStr

	rows, err := db.Query(qString)
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var b banRanking
		err = rows.Scan(&b.ChampionID, &b.BanAvg, &b.BanCount, &b.BanScore)
		if !checkError(err) {
			bs = append(bs, b)
		}
	}

	return bs
}

func getBanList(whereStr string) []gameBan {
	var bans []gameBan
	qString := "SELECT gb.game_id, gb.side, gb.ban_number, gb.champion_id from game_bans gb JOIN game_main gm on gb.game_id = gm.id JOIN match_main mm on gm.match_id = mm.id JOIN tournament_main tm on mm.tournament_id = tm.id JOIN game_stats gs on gm.id = gs.game_id AND gb.side = gs.side"
	fmt.Println(qString + whereStr)
	rows, err := db.Query(qString + whereStr)
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var b gameBan
		err = rows.Scan(&b.GameID, &b.Side, &b.BanNumber, &b.ChampionID)
		if !checkError(err) {
			bans = append(bans, b)
		}
	}

	return bans
}
