package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

func checkForDatabase(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if db == nil {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.WriteHeader(http.StatusServiceUnavailable)
		} else {
			h.ServeHTTP(w, r)
		}
	})
}

func verifyPassword(hash, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

/*
verifyAuthentication is a http.HandlerFunc wrapper which allows us to verify auth on any handler Func prior to calling it.
Without auth -> Handle("/route",myFunc)
With auth -> Handle("/route",verifyAuthentication(myFunc))

verifyAuthentication will return a 403 error if the username + password in the Authentication header is not a match
It will run the parameter http.HandlerFunc if username + password are valid
*/
func verifyAuthentication(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		if !ok {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.WriteHeader(http.StatusForbidden)
			return
		}
		hash := getUserHash(username)
		match := verifyPassword(hash, password)
		if match {
			h.ServeHTTP(w, r)
		} else {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.WriteHeader(http.StatusForbidden)
		}

	})
}

func regionIndex(w http.ResponseWriter, r *http.Request) {
	regions := getAllRegions()
	sendPayload(w, r, regions)

}

func regionLookup(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	regionID := vars["region"]
	reg := getRegion(regionID)
	sendPayload(w, r, reg)
}

func teamLookupByRegion(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	regionID := vars["region"]
	teams := getTeamsByRegion(regionID)
	sendPayload(w, r, teams)
}

func teamIndex(w http.ResponseWriter, r *http.Request) {
	teams := getAllTeams()
	sendPayload(w, r, teams)
}

func teamLookup(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	teamID := vars["team"]

	t := getTeam(teamID)
	sendPayload(w, r, t)
}

func matchInsert(w http.ResponseWriter, r *http.Request) {
	var m match

	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&m)
	if err != nil {
		sendPayload(w, r, err)
		return
	}

	mID, err := insertMatch(m)

	if err != nil {
		fmt.Println("Error during match insert: ", err)
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		m.ID = mID
		sendPayload(w, r, m)
	}
}

func matchUpdate(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotImplemented)
}

func handleOptions(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Allow", "POST, GET")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET")
	w.Header().Set("Access-Control-Allow-Headers", "content-type, authorization")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
}

func matchIndex(w http.ResponseWriter, r *http.Request) {
	matches := getAllMatches()
	sendPayload(w, r, matches)
}

func tournamentIndex(w http.ResponseWriter, r *http.Request) {
	tourns := getAllTournaments()
	sendPayload(w, r, tourns)
}

func clistLookup(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	clistName := vars["name"]

	clistValues := getClistValues(clistName)
	sendPayload(w, r, clistValues)
}

// func gameInsert(w http.ResponseWriter, r *http.Request) {
// 	var g game

// 	dec := json.NewDecoder(r.Body)
// 	err := dec.Decode(&g)
// 	if err != nil {
// 		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
// 		w.Header().Set("Access-Control-Allow-Origin", "*")
// 		w.WriteHeader(http.StatusBadRequest)
// 		if e := json.NewEncoder(w).Encode(err); e != nil {
// 			panic(e)
// 		}
// 		return
// 	}

// 	gID, err := insertGame(g)

// 	if err != nil {
// 		fmt.Println("Error during game insert: ", err)
// 		w.Header().Set("Access-Control-Allow-Origin", "*")
// 		w.WriteHeader(http.StatusInternalServerError)
// 	} else {
// 		g.ID = gID
// 		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
// 		w.Header().Set("Access-Control-Allow-Origin", "*")
// 		w.WriteHeader(http.StatusCreated)
// 		if err := json.NewEncoder(w).Encode(g); err != nil {
// 			panic(err)
// 		}
// 	}
// }

func gameIndex(w http.ResponseWriter, r *http.Request) {
	games := getAllGames()
	sendPayload(w, r, games)
}

func sendPayload(w http.ResponseWriter, r *http.Request, p payload) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(p); err != nil {
		panic(err)
	}
}

func getBanRankingsList(w http.ResponseWriter, r *http.Request) {
	queryVals := r.URL.Query()
	whereStr := "WHERE "

	if len(queryVals["region"]) > 0 {
		whereStr = whereStr + "tm.region_id IN ('" + strings.Join(queryVals["region"], "','") + "')"
	} else {
		whereStr = whereStr + "tm.region_id LIKE '%'"
	}

	rankings := getBanRankings(whereStr)
	sendPayload(w, r, rankings)
}

func banIndex(w http.ResponseWriter, r *http.Request) {
	queryVals := r.URL.Query()
	whereStr := ""

	whereStr = addFilterString(queryVals["region"], whereStr, "tm.region_id")
	whereStr = addFilterString(queryVals["side"], whereStr, "gb.side")
	whereStr = addFilterString(queryVals["champion"], whereStr, "gb.champion_id")
	whereStr = addFilterString(queryVals["split"], whereStr, "tm.split")
	whereStr = addFilterString(queryVals["year"], whereStr, "tm.year")
	whereStr = addFilterString(queryVals["match_type"], whereStr, "mm.match_type")
	whereStr = addFilterString(queryVals["week"], whereStr, "mm.week")
	whereStr = addFilterString(queryVals["team"], whereStr, "gs.team_id")

	if len(whereStr) > 0 {
		whereStr = " WHERE " + whereStr
	}
	bans := getBanList(whereStr)

	sendPayload(w, r, bans)
}

func addFilterString(sl []string, w string, col string) string {
	res := w
	if len(sl) > 0 {
		if len(w) > 0 {
			res = res + " AND "
		}
		res = res + col + " IN ('" + strings.Join(sl, "','") + "')"
	}
	return res
}
