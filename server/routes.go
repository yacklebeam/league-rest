package main

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

type route struct {
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
	OptionsFunc http.HandlerFunc
}

type endpoint struct {
	Description string `json:"description"`
	Pattern     string `json:"url"`
}

type routes []route

var restRoutes routes
var optionMethods map[string]string

func newRouter() *mux.Router {
	optionMethods = make(map[string]string)

	// update all the handlers to use the switch setup (ALL SHOULD HAVE GET HERE)
	restRoutes = routes{
		route{"GET", "regions", regionIndex, handleOptions},
		route{"GET+POST", "matches", matchesHandler, handleOptionsPost},
		route{"GET+PUT", "matches/{match}", matchHandler, handleOptionsPut},
		route{"GET", "tournaments", tournamentIndex, handleOptions},
		route{"GET", "teams", teamIndex, handleOptions},
		route{"POST", "login", verifyLogin, handleOptionsPost},
		route{"POST", "logout", doLogout, handleOptionsPost},
		route{"GET", "metrics", getMetricsHandler, handleOptions},
		route{"POST", "users", userHandler, handleOptionsPost},
	}

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range restRoutes {
		methods := strings.Split(route.Method, "+")
		for _, method := range methods {
			if method == "" {
				continue
			}
			router.
				Methods(method).
				Path("/api/" + route.Pattern).
				Name(method + "_" + route.Pattern).
				Handler(checkForDatabase(route.HandlerFunc))
		}

		// Add the options handler to each endpoint
		optionMethods["/api/"+route.Pattern] = strings.Replace(route.Method, "+", ", ", -1)
		router.
			Methods("OPTIONS").
			Path("/api/" + route.Pattern).
			Name("OPTIONS_" + route.Pattern).
			Handler(route.OptionsFunc)
	}

	return router
}
