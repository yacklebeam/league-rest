package main

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

func checkForDatabase(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if db == nil {
			sendStatus(w, http.StatusServiceUnavailable)
		} else {
			h.ServeHTTP(w, r)
		}
	})
}

func verifyPassword(hash, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func getHash(password string) ([]byte, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return hash, err
}

/*
verifyAuthentication is a http.HandlerFunc wrapper which allows us to verify auth on any handler Func prior to calling it.
Without auth -> Handle("/route",myFunc)
With auth -> Handle("/route",verifyAuthentication(myFunc))

verifyAuthentication will return a 403 error if the username + password in the Authentication header is not a match
It will run the parameter http.HandlerFunc if username + password are valid
*/
/*func verifyAuthentication(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		if !ok {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.WriteHeader(http.StatusForbidden)
			return
		}
		hash := getUserHash(username)
		match := verifyPassword(hash, password)
		if match {
			h.ServeHTTP(w, r)
		} else {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.WriteHeader(http.StatusForbidden)
		}

	})
}*/

func verifyLogin(w http.ResponseWriter, r *http.Request) {
	var u user
	username, password, ok := r.BasicAuth()
	if !ok {
		sendStatus(w, http.StatusForbidden)
		return
	}
	hash := getUserHash(username)
	if verifyPassword(hash, password) {
		u.Token = randToken()
		u.Username = username
		err := loginUser(u.Username, u.Token)
		if err != nil {
			sendStatus(w, http.StatusInternalServerError)
		} else {
			sendPayload(w, u)
		}
	} else {
		sendStatus(w, http.StatusForbidden)
	}
}

func verifyAuthorization(token string) bool {
	return checkUserToken(token)
}

func userHandler(w http.ResponseWriter, r *http.Request) {
	username, password, ok := r.BasicAuth()
	if !ok {
		sendStatus(w, http.StatusForbidden)
		return
	}
	var u user

	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&u)
	if err != nil {
		sendStatus(w, http.StatusInternalServerError)
		return
	}

	hash, err := getHash(password)
	if err != nil {
		sendStatus(w, http.StatusInternalServerError)
		return
	}
	if !useRegistrationKey(u.Token) {
		sendStatus(w, http.StatusInternalServerError)
		return
	}
	userid, err := registerUser(u.Username, hash)
	if err != nil || userid != username {
		sendStatus(w, http.StatusInternalServerError)
		return
	}

	sendStatus(w, http.StatusOK)
}

func doLogout(w http.ResponseWriter, r *http.Request) {
	authHeader := r.Header.Get("Authorization")
	authParts := strings.Split(authHeader, " ")
	if authParts[0] != "Bearer" || authParts[1] == "" {
		sendStatus(w, http.StatusUnauthorized)
		return
	}

	if !verifyAuthorization(authParts[1]) {
		sendStatus(w, http.StatusForbidden)
		return
	}

	var u user

	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&u)
	if err != nil {
		sendStatus(w, http.StatusInternalServerError)
		return
	}

	if u.Token != authParts[1] {
		sendStatus(w, http.StatusForbidden)
		return
	}

	err = logoutUser(u.Username, u.Token)
	if err != nil {
		sendStatus(w, http.StatusInternalServerError)
		return
	}

	sendStatus(w, http.StatusOK)
}

func sendStatus(w http.ResponseWriter, statusCode int) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(statusCode)
}

func randToken() string {
	b := make([]byte, 32)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func regionIndex(w http.ResponseWriter, r *http.Request) {
	regions := getAllRegions()
	sendPayload(w, regions)
}

func matchesHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		matches := getAllMatches()
		sendPayload(w, matches)
	case http.MethodPost:
		authHeader := r.Header.Get("Authorization")
		authParts := strings.Split(authHeader, " ")
		if authParts[0] != "Bearer" || authParts[1] == "" {
			sendStatus(w, http.StatusUnauthorized)
			return
		}

		if !verifyAuthorization(authParts[1]) {
			sendStatus(w, http.StatusForbidden)
			return
		}

		var m match

		dec := json.NewDecoder(r.Body)
		err := dec.Decode(&m)
		if err != nil {
			sendStatus(w, http.StatusInternalServerError)
			return
		}
		newID, err := insertMatch(m)
		if err != nil {
			sendStatus(w, http.StatusInternalServerError)
			return
		}
		m.ID = newID
		sendPayload(w, m)
	default:
		sendStatus(w, http.StatusNotImplemented)
	}
}

func matchHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	matchID, err := strconv.Atoi(vars["match"])
	if err != nil {
		sendStatus(w, http.StatusNotFound)
		return
	}

	switch r.Method {
	case http.MethodGet:
		match := getMatch(matchID)
		if match.ID == -1 {
			sendStatus(w, http.StatusNotFound)
			return
		}
		sendPayload(w, match)
	case http.MethodPut:
		authHeader := r.Header.Get("Authorization")
		authParts := strings.Split(authHeader, " ")
		if authParts[0] != "Bearer" || authParts[1] == "" {
			sendStatus(w, http.StatusUnauthorized)
			return
		}

		if !verifyAuthorization(authParts[1]) {
			sendStatus(w, http.StatusForbidden)
			return
		}

		var m match
		dec := json.NewDecoder(r.Body)
		err = dec.Decode(&m)
		if err != nil {
			sendPayloadWithStatus(w, err, http.StatusInternalServerError)
			return
		}
		err := updateMatch(m, matchID)
		if err != nil {
			sendPayloadWithStatus(w, err, http.StatusInternalServerError)
			return
		}
		sendPayload(w, m)
	default:
		sendStatus(w, http.StatusNotImplemented)
	}
}

func tournamentIndex(w http.ResponseWriter, r *http.Request) {
	tournaments := getAllTournaments()
	sendPayload(w, tournaments)
}

func teamIndex(w http.ResponseWriter, r *http.Request) {
	teams := getAllTeams()
	sendPayload(w, teams)
}

func getMetricsHandler(w http.ResponseWriter, r *http.Request) {
	var dcs []datasetCount
	dcs = getDatasetCount()
	sendPayload(w, dcs)
}

func handleOptions(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Allow", "GET")
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	w.Header().Set("Access-Control-Allow-Headers", "content-type, authorization")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
}

func handleOptionsPost(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Allow", "GET, POST")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST")
	w.Header().Set("Access-Control-Allow-Headers", "content-type, authorization")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
}

func handleOptionsPut(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Allow", "GET, PUT")
	w.Header().Set("Access-Control-Allow-Methods", "GET, PUT")
	w.Header().Set("Access-Control-Allow-Headers", "content-type, authorization")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
}

func sendPayloadWithStatus(w http.ResponseWriter, p payload, status int) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(status)
	if err := json.NewEncoder(w).Encode(p); err != nil {
		panic(err)
	}
}

func sendPayload(w http.ResponseWriter, p payload) {
	sendPayloadWithStatus(w, p, http.StatusOK)
}
