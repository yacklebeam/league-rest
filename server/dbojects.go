package main

type payload interface {
}

type region struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	SelfLink string `json:"self"`
}

type tournament struct {
	ID       int    `json:"id"`
	Region   string `json:"region"`
	Split    string `json:"split"`
	Year     int    `json:"year"`
	SelfLink string `json:"self"`
}

type team struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Region   string `json:"region"`
	SelfLink string `json:"self"`
}

type match struct {
	ID         int    `json:"id"`
	TeamOne    string `json:"teamOne"`
	TeamTwo    string `json:"teamTwo"`
	Tournament int    `json:"tournament"`
	Week       string `json:"week"`
	GameCount  int    `json:"gameCount"`
	SelfLink   string `json:"self"`
}

type user struct {
	Username string `json:"username"`
	Token    string `json:"token"`
}

type datasetCount struct {
	Name  string `json:"name"`
	Count int    `json:"count"`
}
