package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/mux"

	_ "github.com/gorilla/handlers"
)

var db *sql.DB
var router *mux.Router

func checkError(e error) bool {
	if e != nil {
		fmt.Println(e)
		return true
	}
	return false
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("usage: league-rest.exe user/pass@host/dbname")
		return
	}

	argsString := os.Args[1]
	argsString = strings.Replace(argsString, "@", "/", -1)

	parts := strings.Split(argsString, "/")
	if len(parts) < 4 {
		fmt.Println("usage: league-rest.exe user/pass@host/dbname")
		return
	}

	connStr := "user=" + parts[0] + " password=" + parts[1] + " dbname=" + parts[3] + " host=" + parts[2] + " sslmode=disable"

	db = connectToDB(connStr)
	router = newRouter()
	log.Fatal(http.ListenAndServe(":8081", router))
}
