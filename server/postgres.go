package main

import (
	"database/sql"
	"strconv"
	"time"

	_ "github.com/lib/pq"
)

var selectAllRegionsQuery = "SELECT id, name FROM regions"
var selectAllTournamentsQuery = "SELECT id, region, split, year FROM tournaments"
var selectAllTeamsQuery = "SELECT id, name, region FROM teams"
var selectAllMatchesQuery = "SELECT id, team_one, team_two, tournament, week, game_count FROM matches"
var selectMatchQuery = "SELECT id, team_one, team_two, tournament, week, game_count FROM matches WHERE id=$1"
var selectHashByUsernameQuery = "SELECT password_hash FROM users WHERE userid=$1"
var insertMatchQuery = "INSERT INTO matches (team_one, team_two, tournament, week, game_count) VALUES ($1, $2, $3, $4, $5) RETURNING id"
var updateMatchQuery = "UPDATE matches SET team_one=$1, team_two=$2, tournament=$3, week=$4, game_count=$5 WHERE id=$6 RETURNING id"
var updateUserTokenQuery = "UPDATE users SET token=$1, expiration=$2 WHERE userid=$3 RETURNING userid"
var logoutUserQuery = "UPDATE users SET expiration=$1 WHERE userid=$2 and token=$3 RETURNING userid"
var selectTokenByTokenQuery = "SELECT token, expiration FROM USERS WHERE token=$1"
var insertUserQuery = "INSERT INTO users (userid, password_hash) VALUES ($1, $2) RETURNING userid"
var selectRegistrationKeyQuery = "SELECT registration_key FROM registration_keys WHERE registration_key=$1"
var updateRegistrationKeyQuery = "UPDATE registration_keys SET used=1 WHERE registration_key=$1 RETURNING registration_key"

// Metric Queries
var countTableRowsQuery = "SELECT COUNT(*) FROM "

func connectToDB(connStr string) *sql.DB {
	db, err := sql.Open("postgres", connStr)
	if checkError(err) {
		return nil
	}
	return db
}

func useRegistrationKey(key string) bool {
	var usedKey string
	_ = db.QueryRow(selectRegistrationKeyQuery, key).Scan(&usedKey)
	if usedKey == key {
		err := db.QueryRow(updateRegistrationKeyQuery, key).Scan(&usedKey)
		if err != nil {
			return false
		}
		return true
	}
	return false
}

func getDatasetCount() []datasetCount {
	var ds []datasetCount
	ds = append(ds, datasetCount{"Regions", getTableRowCount("regions")})
	ds = append(ds, datasetCount{"Teams", getTableRowCount("teams")})
	ds = append(ds, datasetCount{"Tournaments", getTableRowCount("tournaments")})
	ds = append(ds, datasetCount{"Matches", getTableRowCount("matches")})
	ds = append(ds, datasetCount{"Games", getTableRowCount("games")})

	return ds
}

func loginUser(username string, token string) error {
	var userNameVal string
	expiration := time.Now().Add(1 * time.Hour)
	err := db.QueryRow(updateUserTokenQuery, token, expiration, username).Scan(&userNameVal)
	if err != nil {
		return err
	}

	return nil
}

func logoutUser(username string, token string) error {
	var userNameVal string
	expiration := time.Time{}
	err := db.QueryRow(logoutUserQuery, expiration, username, token).Scan(&userNameVal)
	if err != nil {
		return err
	}

	return nil
}

func registerUser(username string, password []byte) (string, error) {
	var newUserid string
	err := db.QueryRow(insertUserQuery, username, password).Scan(&newUserid)
	if err != nil {
		return "", err
	}
	return newUserid, nil
}

func checkUserToken(token string) bool {
	var tokenBack string
	var expiration time.Time
	_ = db.QueryRow(selectTokenByTokenQuery, token).Scan(&tokenBack, &expiration)
	isAuthorized := tokenBack == token

	if time.Now().After(expiration) {
		isAuthorized = false
	}

	return isAuthorized
}

func getUserHash(username string) string {
	var hash string
	_ = db.QueryRow(selectHashByUsernameQuery, username).Scan(&hash)
	return hash
}

func getAllRegions() []region {
	var rs []region

	rows, err := db.Query(selectAllRegionsQuery)
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var r region
		err = rows.Scan(&r.ID, &r.Name)
		if !checkError(err) {
			r.SelfLink = "/api/regions/" + r.ID
			rs = append(rs, r)
		}
	}

	return rs
}

func getAllTournaments() []tournament {
	var ts []tournament

	rows, err := db.Query(selectAllTournamentsQuery)
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var t tournament
		err = rows.Scan(&t.ID, &t.Region, &t.Split, &t.Year)
		if !checkError(err) {
			t.SelfLink = "/api/tournaments/" + strconv.Itoa(t.ID)
			ts = append(ts, t)
		}
	}

	return ts
}

func getAllTeams() []team {
	var ts []team

	rows, err := db.Query(selectAllTeamsQuery)
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var t team
		err = rows.Scan(&t.ID, &t.Name, &t.Region)
		if !checkError(err) {
			t.SelfLink = "/api/teams/" + t.ID
			ts = append(ts, t)
		}
	}

	return ts
}

func getAllMatches() []match {
	var ms []match

	rows, err := db.Query(selectAllMatchesQuery)
	_ = checkError(err)
	defer rows.Close()

	for rows.Next() {
		var m match
		err = rows.Scan(&m.ID, &m.TeamOne, &m.TeamTwo, &m.Tournament, &m.Week, &m.GameCount)
		if !checkError(err) {
			m.SelfLink = "/api/matches/" + strconv.Itoa(m.ID)
			ms = append(ms, m)
		}
	}

	return ms
}

func getMatch(matchID int) match {
	rows, err := db.Query(selectMatchQuery, matchID)
	_ = checkError(err)
	defer rows.Close()

	var m match
	m.ID = -1

	for rows.Next() {
		err = rows.Scan(&m.ID, &m.TeamOne, &m.TeamTwo, &m.Tournament, &m.Week, &m.GameCount)
		if !checkError(err) {
			m.SelfLink = "/api/matches/" + strconv.Itoa(m.ID)
			return m
		}
	}

	return m
}

func updateMatch(m match, matchID int) error {
	var updMatchID int
	err := db.QueryRow(updateMatchQuery, m.TeamOne, m.TeamTwo, m.Tournament, m.Week, m.GameCount, matchID).Scan(&updMatchID)
	if err != nil {
		return err
	}
	return nil
}

func insertMatch(m match) (int, error) {
	var newMatchID int
	err := db.QueryRow(insertMatchQuery, m.TeamOne, m.TeamTwo, m.Tournament, m.Week, m.GameCount).Scan(&newMatchID)
	if err != nil {
		return -1, err
	}
	return newMatchID, nil
}

// General functions
func getTableRowCount(tableName string) int {
	count := 0
	// can't use a bind variable for the table name...
	err := db.QueryRow(countTableRowsQuery + tableName).Scan(&count)
	if err != nil {
		return -1
	}
	return count
}
