GET ALL REGIONS
/api/region
{
  "values": [
    {
      "id": "NALCS",
      "region_name": "North American League Championship Series",
      "self": "/api/region/NALCS"
    },
    {
      "id": "EULCS",
      "region_name": "European League Championship Series",
      "self": "/api/region/EULCS"
    },
    {
      "id": "LCK",
      "region_name": "League Champions Korea",
      "self": "/api/region/LCK"
    },
    {
      "id": "LPL",
      "region_name": "League of Legends Pro League",
      "self": "/api/region/LPL"
    },
    {
      "id": "LMS",
      "region_name": "League of Legends Master Series",
      "self": "/api/region/LMS"
    }
  ],
  "count": 5
}

GET A REGION
/api/region/LCK
{
  "id": "LCK",
  "region_name": "League Champions Korea",
  "self": "/api/region/LCK"
}

GET ALL TEAMS IN REGION
/api/region/LCK/team
{
  "values": [
    {
      "id": "SKT",
      "team_name": "SK telecom T1",
      "region_id": "LCK",
      "self": "/api/team/SKT"
    },
    {
      "id": "KZ",
      "team_name": "KING-ZONE DragonX",
      "region_id": "LCK",
      "self": "/api/team/KZ"
    },
...
  ],
  "count": 10
}

GET ALL TEAMS
/api/team
{
  "values": [
    {
      "id": "CLG",
      "team_name": "Counter Logic Gaming",
      "region_id": "NALCS",
      "self": "/api/team/CLG"
    },
    {
      "id": "TSM",
      "team_name": "Team SoloMid",
      "region_id": "NALCS",
      "self": "/api/team/TSM"
    },
...
  ],
  "count": 48
}

GET A TEAM
/api/team/SKT
{
  "id": "SKT",
  "team_name": "SK telecom T1",
  "region_id": "LCK",
  "self": "/api/team/SKT"
}