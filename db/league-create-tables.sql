-- Contains all region specific data
CREATE TABLE regions (
  datetime_insert TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
  id VARCHAR(5) PRIMARY KEY NOT NULL,
  name VARCHAR(50)
);

-- Contains all team specific data
CREATE TABLE teams (
  datetime_insert TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
  id VARCHAR(5) PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  region VARCHAR(5) REFERENCES regions(id)
);

-- Contains all tournament specific data
CREATE TABLE tournaments (
  datetime_insert TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
  id SERIAL PRIMARY KEY,
  region VARCHAR(5) references regions(id),
  year INT,
  split VARCHAR(20),
  UNIQUE(region, year, split)
);


-- Contains all data unique to a match (a group of games)
CREATE TABLE matches (
  datetime_insert TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
  id SERIAL PRIMARY KEY,
  team_one VARCHAR(5) NOT NULL REFERENCES teams(id),
  team_two VARCHAR(5) NOT NULL REFERENCES teams(id),
  tournament INT REFERENCES tournaments(id),
  week VARCHAR(20),
  game_count INT DEFAULT 1,
  UNIQUE(tournament, team_one, team_two, week)
);

CREATE TABLE games (
  datetime_insert TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
  id SERIAL PRIMARY KEY NOT NULL,
  match INT REFERENCES matches(id),
  number INT,
  winner VARCHAR(5),
  game_time int
);

-- Contains all data unique to a specific game
CREATE TABLE game_stats (
  datetime_insert TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
  id SERIAL PRIMARY KEY NOT NULL,
  game INT REFERENCES games(id),
  side VARCHAR(5) REFERENCES clist_sides(id),
  team VARCHAR(5) NOT NULL REFERENCES teams(id),
  gold NUMERIC(5, 2),
  kills INT,
  towers INT,
  inhibs INT,
  barons INT,
  dragons INT,
  UNIQUE(game, side)
);

-- Game Bans
CREATE TABLE bans (
  datetime_insert TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
  id SERIAL PRIMARY KEY NOT NULL,
  game INT REFERENCES games(id),
  side VARCHAR(5) REFERENCES clist_sides(id),
  number INT,
  champion VARCHAR(20) REFERENCES clist_champions(id),
  UNIQUE(game, side, number)
);


-- Game Picks
CREATE TABLE picks (
  datetime_insert TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
  id SERIAL PRIMARY KEY NOT NULL,
  game INT REFERENCES games(id),
  side VARCHAR(5) REFERENCES clist_sides(id),
  number INT,
  champion VARCHAR(20) REFERENCES clist_champions(id),
  role VARCHAR(10) REFERENCES clist_roles(id),
  player VARCHAR(20) REFERENCES clist_players(id),
  UNIQUE(game, side, number)
);

-- Lookups
CREATE TABLE clist_champions (
  id VARCHAR(20) PRIMARY KEY NOT NULL,
  description VARCHAR(50)
);

CREATE TABLE clist_match_types (
  id VARCHAR(20) PRIMARY KEY NOT NULL,
  description VARCHAR(50)
);

CREATE TABLE clist_sides (
  id VARCHAR(20) PRIMARY KEY NOT NULL,
  description VARCHAR(50)
);

CREATE TABLE clist_players (
  id VARCHAR(20) PRIMARY KEY NOT NULL,
  description VARCHAR(50)
);

CREATE TABLE clist_roles (
  id VARCHAR(20) PRIMARY KEY NOT NULL,
  description VARCHAR(50)
);

-- Users
CREATE TABLE users (
  datetime_insert TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
  userid VARCHAR(20) PRIMARY KEY NOT NULL,
  password_hash VARCHAR(50) NOT NULL,
  token VARCHAR(50),
  expiration TIMESTAMP WITHOUT TIME ZONE
);