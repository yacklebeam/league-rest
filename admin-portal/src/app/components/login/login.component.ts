import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { LoginService } from '../../services/login.service'
import { ConfigService } from '../../services/config.service' 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  title = 'login';
  username: string = "";
  password: string = "";
  status: string = "";

  constructor(private service: LoginService,
              private config: ConfigService,
              private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.config.setUserName(this.username);
    this.config.setUserToken(""); // Make sure to clear it out!

    this.service.postLogin(this.username, this.password)
      .subscribe(u => 
        {
          this.config.setUserName(u.username);
          this.config.setUserToken(u.token);
          
          this.username = "";
          this.password = "";
          if(this.config.getUserToken() == "" || this.config.getUserToken() == undefined) {
            this.status = "Invalid username or password"
          } else {
            this.status = "";
            this.router.navigate(['/dashboard'])
          }
        }
      );
  }
}
