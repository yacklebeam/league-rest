import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { LoginService } from '../../services/login.service'
import { User, ConfigService } from '../../services/config.service' 

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [LoginService]
})
export class RegisterComponent implements OnInit {
  title = 'register';
  username: string = "";
  password: string = "";
  status: string = "";
  registrationKey: string = "";
  confirmPassword: string = "";

  constructor(private service: LoginService,
              private config: ConfigService,
              private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    let u = {
      username: this.username,
      token: this.registrationKey
    }

    this.service.postUser(u, this.password)
      .subscribe(u => 
        {
          this.router.navigate(['/login'])
        }
      );
  }
}
