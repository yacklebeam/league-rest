import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ConfigService } from '../../services/config.service' 
import { Match } from "../../models/match.model"
import { Observable } from 'rxjs';
import { Team } from 'src/app/models/team.model';
import { DataService } from 'src/app/services/data.service';
import { Tournament } from 'src/app/models/tournament.model';

@Component({
  selector: 'app-edit-match',
  templateUrl: './edit-match.component.html',
  styleUrls: ['./edit-match.component.css'],
  providers: [DataService]
})
export class EditMatchComponent implements OnInit {
  title = 'edit-match';
  selectedMatch: Match;  

  $teams: Observable<Team[]>;
  $tournaments: Observable<Tournament[]>;
  $matches: Observable<Match[]>

  isMatchSelected: boolean = false;
  mode: string = "";

  constructor(private config: ConfigService,
              private api: DataService,
              private router: Router) { }

  ngOnInit(): void {
    if(this.config.getUserToken() == "" || this.config.getUserToken() == undefined) {
      this.router.navigate(['/login']);
    }
    // Clear match for now
    this.clearSelectedMatch();
    this.$teams = this.api.getTeams();
    this.$tournaments = this.api.getTournaments();
    this.$matches = this.api.getMatches();
  }

  clearSelectedMatch() {
    this.selectedMatch = {
      id: -1,
      teamOne: "",
      teamTwo: "",
      tournament: null,
      week: "",
      gameCount: null
    }
  }

  prepNewMatch() {
    this.isMatchSelected = true;
    this.mode = "NEW";
  }

  selectMatch(m: Match) {
    this.selectedMatch = m;
    this.isMatchSelected = true;
    this.mode = "EDIT";
  }

  submit() {
    this.selectedMatch.tournament = Number(this.selectedMatch.tournament)
    if(this.mode == "NEW")
    {
      this.api.postMatch(this.selectedMatch)
      .subscribe(_ => 
        {
          this.$matches = this.api.getMatches();
          this.clearSelectedMatch();
          this.isMatchSelected = false;
          this.mode = "";
        });
    }
    else if (this.mode == "EDIT")
    {
      this.api.putMatch(this.selectedMatch)
      .subscribe(_ => 
        {
          this.$matches = this.api.getMatches();
        });
    }
  }
}
