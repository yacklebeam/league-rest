import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ConfigService } from '../../services/config.service' 
import { DatasetCount, DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [DataService]
})

export class DashboardComponent implements OnInit {

  constructor(private config: ConfigService,
              private router: Router,
              private data: DataService) {  }

  datasetCounts: DatasetCount[];

  ngOnInit() {
    if(this.config.getUserToken() == "" || this.config.getUserToken() == undefined) {
      this.router.navigate(['/login']);
    }

    this.data.getDatasetCounts()
      .subscribe(res => this.datasetCounts = res);
  }

  logout(): void
  {
    this.config.setUserToken("");
    this.config.setUserName("");
    this.router.navigate(['/login']);
  }
}
