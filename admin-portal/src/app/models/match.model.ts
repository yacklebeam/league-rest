export class Match {
    id: number;
    teamOne: string;
    teamTwo: string;
    tournament: number;
    week: string;
    gameCount: number;
}