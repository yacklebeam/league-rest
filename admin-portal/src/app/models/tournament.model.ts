export class Tournament {
    id: number;
    region: string;
    year: number;
    split: string;
}