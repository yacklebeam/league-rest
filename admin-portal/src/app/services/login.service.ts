import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from '../http-error-handler.service';
import { User, ConfigService } from './config.service' 


const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': ''
    })
  };

@Injectable()
export class LoginService {

    private url = 'https://yacklebeam.com/api'
    private handleError: HandleError;

    constructor(
        private http: HttpClient,
        private httpErrorHandler: HttpErrorHandler,
        private config: ConfigService) 
    {
        this.handleError = this.httpErrorHandler.createHandleError('LoginService');
    }

    postLogin(user: string, pass: string): Observable<User>
    {
        var u = new User();
        u.username = this.config.getUserName();
        u.token = this.config.getUserToken();
        
        httpOptions.headers = httpOptions.headers.set('Authorization', 'Basic ' + btoa(user + ":" + pass));
        return this.http.post<User>(this.url + "/login" , u, httpOptions)
            .pipe(
                catchError(this.handleError('login', u))
            );
    }

    postUser(u: User, pass: string): Observable<User>
    {
        httpOptions.headers = httpOptions.headers.set('Authorization', 'Basic ' + btoa(u.username + ":" + pass));
        return this.http.post<User>(this.url + "/users" , u, httpOptions)
            .pipe(
                catchError(this.handleError('register', u))
            );
    }
}