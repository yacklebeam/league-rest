import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from '../http-error-handler.service';
import { Team } from '../models/team.model';
import { Match } from '../models/match.model';
import { Tournament } from '../models/tournament.model';
import { ConfigService } from './config.service';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': ''
    })
};

export class DatasetCount {
    name: string;
    count: number;
}

@Injectable()
export class DataService {
    private url = 'https://yacklebeam.com/api'
    private handleError: HandleError;

    constructor(
        private http: HttpClient,
        private httpErrorHandler: HttpErrorHandler,
        private config: ConfigService) 
    {
        this.handleError = this.httpErrorHandler.createHandleError('DataService');
    }

    getDatasetCounts(): Observable<DatasetCount[]>
    {
        return this.http.get<DatasetCount[]>(this.url + "/metrics", httpOptions)
            .pipe(res => res);
    }

    getTeams(): Observable<Team[]>
    {
        return this.http.get<Team[]>(this.url + "/teams", httpOptions)
            .pipe(res => res);
    }

    getMatches(): Observable<Match[]>
    {
        return this.http.get<Match[]>(this.url + "/matches", httpOptions)
            .pipe(res => res);
    }

    getTournaments(): Observable<Tournament[]>
    {
        return this.http.get<Tournament[]>(this.url + "/tournaments", httpOptions)
            .pipe(res => res);
    }

    postMatch(m: Match): Observable<Match>
    {
        httpOptions.headers = httpOptions.headers.set('Authorization', 'Bearer ' + this.config.getUserToken());
        return this.http.post<Match>(this.url + "/matches" , m, httpOptions)
            .pipe(
                catchError(this.handleError('matches', m))
            );
    }

    putMatch(m: Match): Observable<Match>
    {
        httpOptions.headers = httpOptions.headers.set('Authorization', 'Bearer ' + this.config.getUserToken());
        return this.http.put<Match>(this.url + "/matches/" + m.id , m, httpOptions)
            .pipe(
                catchError(this.handleError('matches/id', m))
            );
    }
}