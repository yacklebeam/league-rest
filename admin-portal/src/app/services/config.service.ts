import { Injectable, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class User {
  username: string;
  token: string;
}

@Injectable()
export class ConfigService {

  private KEY_USERNAME = 'league-stats-admin-portal-username';
  private KEY_TOKEN = 'league-stats-admin-portal-token';

  constructor() { }

  public setUserName(name: string)
  {
    sessionStorage.setItem(this.KEY_USERNAME, name);
  }

  public setUserToken(token: string)
  {
    sessionStorage.setItem(this.KEY_TOKEN, token);
  }

  public getUserName(): string
  {
    return sessionStorage.getItem(this.KEY_USERNAME);
  }

  public getUserToken(): string
  {
    return sessionStorage.getItem(this.KEY_TOKEN);
  }
}
